# Managing sites

Here you can find how you can manage the sites stored on your service.

### Table of Contents

1.  [Delete a site](#delete-a-site)


## Delete a site

The sites can be deleted only if you have edit rights to it. This is happening 
if you `directly contributed to it`, or the site is added to a map where you are
a `leader` or `owner`.

### From the site list 

If you want to delete a site, you first need to find it under the `manage`
section.

You can do this by browsing the `Manage > Sites` section or by browsing the map
sites under `Manage > Maps`.

After you find the Site, you can check the context menu associated to it:

![the site context menu](../../img/manage/sites/site-context-menu.png "the site context menu")

If you select `Delete` a message box will be prompted to confirm the deletion
of the site.

### From the site edit page 

On the site edit page, in the side panel, you can find a delete site button. 

If you press `Delete` a message box will be prompted to confirm the deletion
of the site.