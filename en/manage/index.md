# Data management

Manage your data stored in your app.

## Table of contents

| Topic                   | Description                                        |
| ----------------------- | -------------------------------------------------- |
| [Sites](sites.md)       | How to work with sites                             |
