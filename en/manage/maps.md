# Managing Maps

Here you can find how you can manage the maps stored on your service.

### Table of Contents

1.  [Importing sites](#import-sites)
2.  [Exporting sites](#exporting-sites)


## Importing sites

GIS Collective allows you to import sites to your maps from `CSV` files.

In order to import a file, you have to go to the `Map files` page. The page can be 
accessed by going to `Manage -> Maps` and opening the menu next to the map title:

![the map context menu](../../img/manage/maps/map-context-menu.png "the map context menu")

There, you can upload files, by pressing the upload button:

![the map file upload buton](../../img/manage/maps/map-file-upload-buton.png "the map file upload buton")

Once your files are uploaded, you can trigger the import action from the file dropdown:

![the map file context menu](../../img/manage/maps/map-file-context-menu.png "the map file context menu")

The file will be processed in background and all the valid items will be imported to your map.

### CSV file format

The CSV file must have the following fields, in any order:

#### _id

The site id. If it exists in the database, the existing site will be updated with the csv data

#### maps

A list of map ids that the site will be associated with

#### name

The site name

#### description
    
A text associated to the site. You can use markdown syntax to enrich the format.

#### position.lon

It represenst the longitude of the site. The coordinate must be a `double` value using the `WGS84` CRS.
    
#### position.lat
    
It represenst the latitude of the site. The coordinate must be a `double` value using the `WGS84` CRS. 

#### pictures
    
It contains a list of picture ids associated to the site. The list separator must be the `;` char.
    
Example: 
        ```
        5ca7bfe0ecd8490100cab9d4;5ca7bfd8ecd8490100cab9bf
        ```

#### icons
    
It contains a list of icon ids associated to the site. The list separator must be the `;` char.
    
Example: 
        ```
        5ca7bfe0ecd8490100cab9d4;5ca7bfd8ecd8490100cab9bf
        ```
    
#### isPublished

It must be `true` if the site is public or `false` if it is private

#### contributors
    
A list of user id's or email, of the people who contributed to the site
    
#### attributes
    
Represents the site's attributes. If they are present, they must be a valid Json object



### Things to know

1. If there is already a site with the same `name` or `id`, it will be replaced with the site from the file
2. If the `maps` field does not exist or the id of the map that you want to import is not set, it will be added by default.
3. You can't set maps that you don't have access to, in the `maps` field. Make sure you can add sites to all maps.
4. It's recommended, if possible, to use ids instead of names

### Sample CSV file data

```
"_id","maps","name","description","position.lon","position.lat","info.createdOn","info.lastChangeOn","info.changeIndex","info.author","info.originalAuthor","pictures","icons","isPublished","contributors","attributes"
"5ca8baf3ef1f7e01000849c8","5ca89e37ef1f7e010007f54c","kubus TU Berlin (Kooperations- und Beratungsstelle für Umweltfragen)","Universitärer Wissenschaftsladen:",13.324796,52.517944,"2010-01-05T00:30:00Z","2010-01-05T00:30:00Z",4,"@Frank Becker",,,"5ca7bfe0ecd8490100cab9d4;5ca7bfd8ecd8490100cab9bf","true","5b86efef796da25424540dea;5b870669796da25424540deb","{""Last Edited"":""01/05/2010 - 12:00"",""Awaiting Approval"":"""",""Site Email"":"""",""Entry cost"":"""",""User ID"":"""",""Additional"":""Räume FR 7005, FR 7504, FR 7504A"",""Contributor Email"":"""",""Published"":""Yes"",""Language"":"""",""Video caption or credit"":"""",""Free entry"":""Free entry"",""Authored On"":""01/05/2010 - 10:12"",""Country"":"""",""Wheelchair Accessible"":""Not accessible"",""Children Welcome"":""Not child friendly"",""Public Site"":""Yes"",""Icon"":""Eco Information, Information Kiosk"",""Site ID"":9488,""Primary Term"":""Information Kiosk"",""Latitude"":52.517944,""Street location"":""Franklinstraße 28/ 29"",""Contributor Directly Involved In Site"":""No"",""Details of Volunteer Opportunities"":"""",""Authored By"":"""",""Province"":"""",""Video"":"""",""Image"":"""",""Appointment Needed"":""Appointment not needed"",""Public Transit Directions"":""- vom U - Bhf. Ernst Reuter Platz über die Marchstraße mit ca. 10 Minuten Fußweg,\r\n- vom S - Bhf. Tiergarten über die Straße des 17.Juni und das Salzufer in ca. 15 Minuten zu Fuß,\r\n- mit dem Bus 245 (Zoologischer Garten <> Nordbahnhof) bis zur Haltestelle Marchbrücke"",""Contributor Name"":""Frank Becker"",""Accessible by public transport"":""Not accessible by public transport"",""Volunteer"":""No Volunteer Opportunities"",""Maps"":""Greenmap Berlin"",""Postal Code"":10587,""Map IDs"":5901,""City"":""Berlin"",""Image caption or credit"":"""",""Telephone"":"""",""Tags"":"""",""Web Address"":"""",""Longitude"":13.324796}"
"5ca8baf5ef1f7e01000849dc","5ca89e37ef1f7e010007f54c","Fördergemeinschaft Ökologischer Landbau Berlin-Brandenburg e.V. (FÖL)","Die Fördergemeinschaft Ökologischer Landbau Berlin-Brandenburg e.V. ist ein gemeinnütziger Verein, der die Nachfrage nach Bio-Produkten mit überverbandlicher Verbraucherinformation und Öffentlichkeitsarbeit fördert. ",13.383716,52.522242,"2010-04-12T00:30:00Z","2010-05-01T00:30:00Z",5,"5ca90cbc82ff92d26a7edf5f","5ca90cbc82ff92d26a7edf5f",,"5ca7bfe0ecd8490100cab9d4;5ca7bfd2ecd8490100cab9b3;5ca7bfd5ecd8490100cab9bb;5ca7bfb7ecd8490100cab972","true","5b86efef796da25424540dea;5b870669796da25424540deb","{""Last Edited"":""05/01/2010 - 07:28"",""Awaiting Approval"":"""",""Site Email"":""info@foel.de"",""Entry cost"":"""",""User ID"":2510,""Additional"":"""",""Contributor Email"":""m.fuessel@foel.de"",""Published"":""Yes"",""Language"":"""",""Video caption or credit"":"""",""Free entry"":""Free entry"",""Authored On"":""04/12/2010 - 08:42"",""Country"":""Germany"",""Wheelchair Accessible"":""Not accessible"",""Children Welcome"":""Not child friendly"",""Public Site"":""Yes"",""Icon"":""Eco Information, Eco Club/Organization, Significant Organization/Agency, Social Enterprise"",""Site ID"":13206,""Primary Term"":""Significant Organization/Agency"",""Latitude"":52.522242,""Street location"":""Marienstraße 19-20"",""Contributor Directly Involved In Site"":""Yes"",""Details of Volunteer Opportunities"":"""",""Authored By"":""foel"",""Province"":"""",""Video"":"""",""Image"":"""",""Appointment Needed"":""Appointment not needed"",""Public Transit Directions"":""S- und U-Bahn-Station Friedrichsstraße"",""Contributor Name"":""Mark Füssel"",""Accessible by public transport"":""Accessible by public transport"",""Volunteer"":""No Volunteer Opportunities"",""Maps"":""Greenmap Berlin"",""Postal Code"":10117,""Map IDs"":5901,""City"":""Berlin"",""Image caption or credit"":"""",""Telephone"":""030 - 28 48 24 40"",""Tags"":""Bio-Berlin-Brandenburg;Fördergemeinschaft Ökologischer Landbau;Verein;FÖL;regional;gemeinnützig;Bio-Einkaufsführer;Bio Adressen online"",""Web Address"":""http://www.bio-berlin-brandenburg.de"",""Longitude"":13.383716}"

```


## Exporting sites

GIS Collective allows you to export sites to `CSV` files.

In order to export a file, you need to go to the `Manage -> Maps` page and select the `Download CSV` option:

![the map context menu](../../img/manage/maps/map-context-menu.png "the map context menu")
