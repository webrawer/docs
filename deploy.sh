#!/bin/bash

echo "Deploy docs: $1"
echo "========================================"

rm -r /srv/$1/content/en || true
cp -r ./en /srv/$1/content

rm -r /srv/$1/content/img || true
cp -r ./img /srv/$1/content
